import React, { Component } from 'react';
import './ToDoListItem.css';

// やってること単純でToDoListItemっていうオブジェクトの定義をここでしているだけ。
class ToDoListItem extends Component {
  render() {
    const {
      title,
      description,
      time,
      ...props
    } = this.props;

    return (
      <div className="ToDoListItem" {...props}>
        <div className="ToDoListItem-title">{title}</div>
        <div className="ToDoListItem-description">{description}</div>
        <div className="ToDoListItem-time">{time}</div>
      </div>
    );
  }
}

export default ToDoListItem;